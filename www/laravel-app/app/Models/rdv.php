<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class rdv extends Model
{
    protected $fillable = array('nom','prenom','email','role','password','photo','matricule','etat',);
    public static $rules = array('nom','prenom','email','role','password','photo','matricule','etat'=>'required');
    public function forms()
    {
        return $this->hasMany('App/froms');
    }

}
